import java.util.Random;
import java.util.Scanner;

/**
 * Created by Alex_Erzhanov on 02.06.2017.
 */


public class Main {

    public static void main(String[] args) {


        //Task 1
        System.out.println("Task 1");
        Scanner numb = new Scanner(System.in); //creating scanner object
        System.out.println("Enter natural number:");
        int n = numb.nextInt(); //reading number from keyboard
            if (n < 0) {
                throw new IllegalArgumentException("You entered negative number!!!");
            }
        MySqrt s = new MySqrt();
        int c = s.getSqrt(n); //calling sqrt metod
        System.out.println("The square root is: " + c + "\n");


        //Task 2
        System.out.println("Task 2\n");
        int[] arr = {8, 5, 4, 6, 7};
        MassiveElement a = new MassiveElement();
        int[] sort = a.getSort(arr);
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter index 1-" + arr.length + ":");
        int k = sc.nextInt();
        System.out.println("Your element is: " + sort[k - 1] + "\n");


        //Task 3
        System.out.println("Task 3\n");
        CsvBuilder csv = new CsvBuilder("Cypher.csv");
        String[] codes = {"RMuiRdf01160141151156164", "lims8r3860lims1631411561441" , "EOcTkerf3891511431450551431621451411550"};
        csv.Build(codes);


        //Task4
        Splain splain = new Splain();
        Random ran = new Random();
        Integer[][] coord = new Integer[10][2];
        for (int i = 0; i < 10; i++) { //fill the array by random numbers for task 4

            for (int j = 0; j < 2; j++) {

                coord[i][j] = ran.nextInt(100);
            }
        }
        System.out.println("\n\nTask 4\n");
        splain.getSplain(coord); //call the method that sorting the points for Splain

    }


}