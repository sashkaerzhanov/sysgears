import com.csvreader.CsvWriter;
import org.json.simple.JSONObject;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Alex_Erzhanov on 02.06.2017.
 */


public class CsvBuilder {

    public static String Cypher = "Шифр";
    public static String DriverCode = "Код водителя";
    public static String ListCode = "Код путевого листа";
    public static String Dangerous = "Опасный";
    public static String Fragile = "Хрупкий";
    public static String Temp = "Температура";
    public static String Name = "Наименование";
    public static boolean FileExist = false;
    public static CsvWriter csvOut = null;
    public static JSONObject result = null;


    public CsvBuilder(String fileName) {

        // before we open the file check to see if it already exists
        FileExist = new File(fileName).exists();

        try {
            // use FileWriter constructor that specifies open for appending
            csvOut = new CsvWriter(new FileWriter(fileName, true), ',');
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void Build(String[] cypher) {

        if (!FileExist) {
            setHeader(); //write header if file doesn't exist
        }

        for (int i = 0; i < cypher.length; i++) {
            System.out.println("Cypher - " + cypher[i]);
            getBody(cypher[i]);
        }
        csvOut.close();
    }

    public void setHeader() { //writing header of csv
        try {
            csvOut.write(Cypher);
            csvOut.write(DriverCode);
            csvOut.write(ListCode);
            csvOut.write(Dangerous);
            csvOut.write(Fragile);
            csvOut.write(Temp);
            csvOut.write(Name);
            csvOut.endRecord();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void getBody(String code) {
        Decoder cypher = new Decoder(); //creating decoder obj
        result = cypher.parseCypher(code); //parsing cypher
        System.out.println(result.toJSONString()); //writing result in console

        if (!result.isEmpty()) {
            //writing into csv
            try {
                csvOut.write(code);
                csvOut.write(result.get("DriveCode").toString());
                csvOut.write(result.get("ListCode").toString());
                csvOut.write(result.get("Dangerous").toString());
                csvOut.write(result.get("Fragile").toString());
                csvOut.write(result.get("Temperature").toString());
                csvOut.write(result.get("Name").toString());
                csvOut.endRecord();
            }
            catch (IOException e) {
                e.printStackTrace();
            }

            result.clear();
        }

    }
}
