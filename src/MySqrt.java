/**
 * Created by Alex_Erzhanov on 01.06.2017.
 */
public class MySqrt {


    public int getSqrt(int n) {
        int res = 0;
        if (n >= 0) {
            do {
                res++; //increment for each loop
            }
            while (res * res <= n); //checking condition
        }
        return --res;
    }
}
