import java.util.Arrays;

/**
 * Created by Alex_Erzhanov on 01.06.2017.
 */
public class Splain {
    void getSplain(Integer[][] randomCoord) {
        System.out.println(Arrays.deepToString(randomCoord) + " List of random coordinates");
        bubbleSort(randomCoord);
        GrahamAlg(randomCoord);
        System.out.println(Arrays.deepToString(randomCoord) + " Coordinates of broken line ");
    }


    private void bubbleSort(Integer[][] arr) {
        /*The outer loop each time reducing the array fragment
        as the inner loop every time puts to the
        end a element with first maximal element*/

        for (int i = arr.length - 1; i > 0; i--) {

            for (int j = 0; j < i; j++) {
                /*Compare first elements in pairs,
                if they have the wrong order,
                we change places*/
                if (arr[j + 1][0] < arr[j][0]) {

                    int tmp1 = arr[j + 1][0];
                    arr[j + 1][0] = arr[j][0];
                    arr[j][0] = tmp1;

                    int tmp2 = arr[j + 1][1];
                    arr[j + 1][1] = arr[j][1];
                    arr[j][1] = tmp2;
                }
            }
        }
    }





    private Integer rotate(Integer[][] a, int i) {
        int res = ((a[i][0] - a[0][0]) * (a[i + 1][1] - a[i][1]) - (a[i][1] - a[0][1]) * (a[i + 1][0] - a[i][0]));
        return res;
        /*Determines where the point C is from the pair AB.
        A positive value corresponds to the left side, a negative value corresponds to the right side*/
    }




    private void GrahamAlg(Integer[][] arr) {
        for (int i = arr.length - 1; i > 0; i--) {

            for (int j = 1; j < i; j++) {
                /*Compare  elements in pairs,
                If the point C is to the left of the pair AB, we change places*/
                if (rotate(arr, j) < 0) {

                    int tmp1 = arr[j + 1][0];
                    arr[j + 1][0] = arr[j][0];
                    arr[j][0] = tmp1;

                    int tmp2 = arr[j + 1][1];
                    arr[j + 1][1] = arr[j][1];
                    arr[j][1] = tmp2;
                }
            }
        }

    }
}
