import org.json.simple.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alex_Erzhanov on 02.06.2017.
 */
public class Decoder {

    public static String cypherRegexp = "([a-zA-Z]{4})+.*((R|r)+?([\\w]{4})([a-zA-Z]{4})?((\\+|\\-)[0-9]{3})?([0-9]*))";
        //regular expression for cypher

    public JSONObject parseCypher(String cypher) {

        JSONObject result = new JSONObject();

        String g1 = "",  g4 = "", g5 = "", g6 = "",  g8 = "";
        //variables for groups
        Pattern p = Pattern.compile(cypherRegexp);//create pattern object that matches the regex
        Matcher m = p.matcher(cypher);//create matcher object that checks string for regular expression

        if (m.find()) {
            //DriveCode
            if (m.group(1) != null ) {
                //If driveCode exist can put to result
                g1 = m.group(1);
                result.put("DriveCode", g1);

                if (m.group(4) != null) {
                    //If ListCode exist can put to result
                    g4 = m.group(4);
                    result.put("ListCode", g4);

                    if (g4.contains("d")) {
                        //d-Dangerous))
                        result.put("Dangerous", true);
                    }

                        else {
                        result.put("Dangerous", ""); //not Dangerous
                        }

                    if (g4.contains("f")) {
                        //fragile
                        result.put("Fragile", true);
                    }

                        else {
                        result.put("Fragile", ""); //not fragile
                        }
                }

                // Drive Code From List
                if (m.group(5) != null) {
                    //If driveCodefrom list exist can put to result
                    g5 = m.group(5);
                    result.put("DriveCodeFromList", g5);
                }

                // Temperature
                if (m.group(6) != null) {
                    //If temperature exist can put to result
                    g6 = m.group(6);
                    result.put("Temperature", g6);
                }
                    else {
                    result.put("Temperature", ""); //temperature is not specified
                    }

                // Name
                if (m.group(8) != null) {
                    g8 = parseName(m.group(8)); //converts the cypher to name
                    result.put("Name", g8);
                }

            }

        }
        else {
            System.out.println("Cypher is incorrect!");
        }

        return result;

    }

    public String parseName(String cypherName) {

        String[] c = cypherName.split("(?<=\\G.{3})");
        //Split a string into an array of substrings / Each substring has 3 symbols

        String name = "";

        for (int i = 0; i <= c.length - 1; i++) {
            if (c[i].length() == 3) {
                int symbol = Integer.valueOf(c[i], 8); //string to int
                char ch = (char) symbol; //int to char
                name = name + ch;
            }
        }

        return name;
    }

}
