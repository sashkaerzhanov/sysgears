/**
 * Created by Alex_Erzhanov on 01.06.2017.
 */
public class MassiveElement {


    public int[] getSort(int[] arr) {

        if (arr == null || arr.length == 0) {

            throw new NullPointerException();

        }

        for (int i = arr.length - 1; i >= 1; i--) {
            /*The outer loop each time reducing the array fragment
            as the inner loop every time puts to the
            end a element with first maximal element*/
            boolean sorted = true;

            for (int j = 0; j < i; j++) {
                /*Compare first elements in pairs,
                if they have the wrong order,
                we change places*/
                if (arr[j] > arr[j + 1]) {

                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                    sorted = false;//changing variable for checking
                }
            }
            if (sorted) { //sorting check
                break;
            }
        }
        return arr;
    }
}
